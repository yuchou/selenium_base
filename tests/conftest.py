#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 18-9-5 下午4:50
# @Author  : yuchou
# @Email   : yuchou87@gmail.com
# @File    : conftest.py
# @desc    : desc
import pytest


@pytest.fixture
def selenium(selenium):
    selenium.implicitly_wait(10)
    selenium.maximize_window()
    return selenium