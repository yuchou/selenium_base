#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 18-9-5 下午9:24
# @Author  : yuchou
# @Email   : yuchou87@gmail.com
# @File    : test_search.py
# @desc    : desc
import time
from pages.Home.homePage import HomePage


def test_search_pass(selenium):
    home = HomePage(selenium)
    home.openUrl('https://www.baidu.com')
    home.search('test')
    time.sleep(1)


def test_search_fail(selenium):
    home = HomePage(selenium)
    home.openUrl('https://www.baidu.com')
    assert 0
