#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 18-9-7 上午8:48
# @Author  : yuchou
# @Email   : yuchou87@gmail.com
# @File    : send_mail.py
# @desc    : desc
import zmail
import os
from datetime import datetime

to_lists = ['xfufo@qq.com']
reports_dir = os.path.join(os.path.abspath(os.path.dirname(os.path.dirname(__file__))), 'reports')


def mail_to(attachments):
    current_time = datetime.strftime(datetime.now(), fmt='%Y-%m-%d %H:%M:%S')

    mail_content = {
        'subject': f'Selenium测试报告 - {current_time}',
        'content': 'Selenium测试报告',
        'attachments': os.path.join(reports_dir, attachments)
    }

    server = zmail.server(user='11111', password='2222')
    server.send_mail(recipients=to_lists, message=mail_content)
