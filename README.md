# selenium_base

## 什么是Selenium-Grid
Selenium-Grid允许您在不同的机器上针对不同的浏览器并行运行测试。也就是说，在运行不同浏览器和操作系统的不同机器上同时运行多个测试。

## 什么时候使用Selenium-Grid
1. 需要在多浏览器、多版本浏览器和不同操作系统上运行上
2. 检查测试套件完成测试所需要的时间

## Selenium-Grid使用

### 命令方式运行

```shell
# 启动hub节点
$ java -jar selenium-server-standalone-2.44.0.jar -role hub

# 启动node节点
$ java -jar selenium-server-standalone-2.44.0.jar -role node  -hub http://localhost:4444/grid/register
```

### Docker方式运行
#### docker-compose.yml
```yml
version: '2'
services:
  firefox:
    image: selenium/node-firefox:3.14.0-beryllium
    volumes:
      - /dev/shm:/dev/shm
    depends_on:
      - hub
    environment:
      HUB_HOST: hub

  chrome:
    image: selenium/node-chrome:3.14.0-beryllium
    volumes:
      - /dev/shm:/dev/shm
    depends_on:
      - hub
    environment:
      HUB_HOST: hub

  hub:
    image: selenium/hub:3.14.0-beryllium
    ports:
      - "4444:4444"
```

#### 启动和调试
```shell
$ docker-compose up
```
> 1.启动成功后，访问 http://ip:4444/grid/console?config=true&configDebug=true ，可以查看hub和node配置调试信息
> 2.建议使用debug镜像，方便远程调试


#### 安装中文语言包
```shell
$ sudo apt-get update && sudo apt-get -y install ttf-wqy-microhei ttf-wqy-zenhei && sudo apt-get clean
```

### 分布式测试示例（python）
```python
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

driver = webdriver.Remote(
    command_executor='http://10.10.0.63:4444/wd/hub',
    desired_capabilities=DesiredCapabilities.FIREFOX
)


driver.get('https://www.baidu.com')
print(driver.title)
driver.find_element_by_id('kw').send_keys('test')
driver.find_element_by_id('su').click()
driver.get_screenshot_as_file('test.png')
driver.close()
```


