#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/8/29 14:24
# @Author  : yuchou
# @Email   : yuchou87@gmail.com
# @File    : demo_chrome.py
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


driver = webdriver.Remote(
    command_executor='http://10.10.0.63:4444/wd/hub',
    desired_capabilities=DesiredCapabilities.CHROME
)


driver.get('http://www.163.com')
print(driver.title)
driver.close()
