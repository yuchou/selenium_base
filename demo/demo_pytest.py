#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 18-9-5 下午4:23
# @Author  : yuchou
# @Email   : yuchou87@gmail.com
# @File    : demo_pytest.py
# @desc    : desc
import pytest
from selenium.webdriver.common.by import By


@pytest.fixture
def selenium(selenium):
    selenium.implicitly_wait(10)
    selenium.maximize_window()
    return selenium


def test_baidu(selenium):
    selenium.get('http://www.baidu.com')
    selenium.find_element(By.ID, 'kw').send_keys('test')
    selenium.find_element(By.ID, 'su').click()
    print(selenium.title)