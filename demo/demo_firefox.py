#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 2018/8/29 14:44
# @Author  : yuchou
# @Email   : yuchou87@gmail.com
# @File    : demo_firefox.py

from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

driver = webdriver.Remote(
    command_executor='http://10.10.0.63:4444/wd/hub',
    desired_capabilities=DesiredCapabilities.FIREFOX
)


driver.get('https://www.baidu.com')
print(driver.title)
driver.find_element_by_id('kw').send_keys('test')
driver.find_element_by_id('su').click()
driver.get_screenshot_as_file('test.png')
driver.close()
