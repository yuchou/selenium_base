#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 18-9-5 下午9:06
# @Author  : yuchou
# @Email   : yuchou87@gmail.com
# @File    : homePage.py
# @desc    : desc
from pages.basePage import BasePage


class HomePage(BasePage):

    _searchFiled = 'kw'
    _searchButton = 'su'

    def __init__(self, selenium):
        super().__init__(selenium)

    def enter_text(self, text):
        self.sendKeys(text, self._searchFiled)

    def search_click(self):
        self.elementClick('id', self._searchButton)

    def search(self, text):
        self.enter_text(text)
        self.search_click()
