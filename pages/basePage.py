#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Time    : 18-9-5 下午4:51
# @Author  : yuchou
# @Email   : yuchou87@gmail.com
# @File    : basePage.py
# @desc    : desc
from selenium.webdriver.common.by import By


class BasePage(object):

    def __init__(self, selenium):
        self.driver = selenium

    def getByType(self, locatorType='id'):
        locatorType = locatorType.lower()

        if locatorType == 'id':
            return By.ID
        elif locatorType == 'name':
            return By.NAME
        elif locatorType == 'xpath':
            return By.XPATH
        elif locatorType == 'css':
            return By.CSS_SELECTOR
        elif locatorType == 'class':
            return By.CLASS_NAME
        elif locatorType == 'link':
            return By.LINK_TEXT
        else:
            print(f'指定的类型：{locatorType} 不支持！')

    def getElement(self, locatorType='id', locator=None):
        element = None

        try:
            locatorType = locatorType.lower()
            byType = self.getByType(locatorType)
            element = self.driver.find_element(byType, locator)
            print('元素找到了！')
        except:
            print('元素未找到！')

        return element

    def elementClick(self, locatorType='id', locator=None):
        try:
            element = self.getElement(locatorType, locator)
            element.click()
            print(f'点击 {locatorType} {locator} 成功！')
        except:
            print(f'点击 {locatorType} {locator} 失败！')

    def sendKeys(self, data, locator="", locatorType="id", element=None):
        try:
            if locator:
                element = self.getElement(locatorType, locator)
            element.send_keys(data)
        except:
            pass

    def openUrl(self, url):
        self.driver.get(url)

    def getTitle(self):
        return self.driver.title

    def webScroll(self, direction='up'):
        if direction == 'down':
            self.driver.execute_script("window.scrollBy(0, 1000);")
        else:
            self.driver.execute_script("window.scrollBy(0, -1000);")